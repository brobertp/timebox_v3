import React from 'react';
import { v4 as uuidv4 } from 'uuid';

import Timebox from './timebox';
import TimeboxCreator from './timebox_creator';

class TimeboxList extends React.Component {

    state = {
        timeboxes: [
            { id: uuidv4(), title:'Timebox 1', totalTimeInMinutes: '23' },
            { id: uuidv4(), title:'Timebox 2', totalTimeInMinutes: '45' },
            { id: uuidv4(), title:'Timebox 3', totalTimeInMinutes: '65' },
            { id: uuidv4(), title:'Timebox 4', totalTimeInMinutes: '13' },
            { id: uuidv4(), title:'Timebox 5', totalTimeInMinutes: '07' },
            { id: uuidv4(), title:'Timebox 6', totalTimeInMinutes: '11' },
        ]
    }

    deleteHandler = (id) => {
        console.info('OnDelete: ', id);
        this.setState((prevState) => {
            const timeboxes = prevState.timeboxes.filter((timebox, index) => timebox.id !== id);
            return {
                timeboxes,
            }
        })
    }

    editHandler = (indexToUpdate, updatedTimebox) => {

        this.setState(prevState => {
            const timeboxes = prevState.timeboxes.map((timebox, index) => {
                return indexToUpdate === timebox.id ? updatedTimebox : timebox;
            })

            return {
                timeboxes,
            };
        })
    }

    addTimebox = (timebox) => {
        this.setState((prevState) => {
            return {
                timeboxes: [ timebox , ...prevState.timeboxes, ]
            }
        });
    }

    createHandler = (createdTimebox) => {
        console.info('createHandler');
        this.addTimebox(createdTimebox);
    }

    render() {

        return (
            <>
                <TimeboxCreator
                    onCreate={this.createHandler}
                />
                {
                    this.state.timeboxes.map((timebox, id) => {
                        return (
                            <Timebox
                                key={timebox.id}
                                title={timebox.title}
                                totalTimeInMinutes={timebox.totalTimeInMinutes}
                                onDelete={() => this.deleteHandler(timebox.id)}
                                onEdit={(changedTimebox) => this.editHandler(timebox.id, { ...timebox, ...changedTimebox })}
                            />);
                    })
                }
            </>
        )
    }
}

export default TimeboxList;

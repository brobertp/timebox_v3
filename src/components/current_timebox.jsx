import React from 'react';

import Clock from './clock';
import ProgressBar from './progressbar';

/*********************************************************************
******************* Functional Component "TimeBox" *******************
**********************************************************************/
class CurrentTimeBox extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isRunning: false,
            isPaused: false,
            pausesCount: 0,
            elapsedTimeInSeconds: 0,
        }
    }

    handleStop = (event) => {
        this.stopTimer();
        this.setState({
            isPaused: false,
            isRunning: false,
            pausesCount: 0,
            elapsedTimeInSeconds: 0
        });
    }

    handleStart = (event) => {
        this.setState({
            isRunning: true,
        });

        this.startTimer();
    }

    togglePause = () => {

        this.setState((prevState) => {
            const isPaused = !prevState.isPaused;

            if (isPaused) {
                this.stopTimer();
            } else {
                this.startTimer();
            }
            return {
                isPaused,
                pausesCount: isPaused ? prevState.pausesCount + 1 : prevState.pausesCount,
            }
        });


    }

    startTimer = () => {
        this.intervalid = setInterval(() => {
            this.setState((prevState) => ({ elapsedTimeInSeconds: prevState.elapsedTimeInSeconds + 1}))
        }, 1000);
    }

    stopTimer = () => {
        clearInterval(this.intervalid);
    }

    render() {

        const { isRunning, isPaused, pausesCount, elapsedTimeInSeconds } = this.state;
        const { title, totalTimeInMinutes, isEditable, onEdit } = this.props;

        const totalTimeInSeconds = totalTimeInMinutes * 60;
        const timeLeftInSeconds = totalTimeInSeconds - elapsedTimeInSeconds;
        const minutesLeft = Math.floor(timeLeftInSeconds / 60);
        const secondsLeft = Math.floor(timeLeftInSeconds % 60);

        const progressInPercent = ( timeLeftInSeconds / totalTimeInSeconds ) * 100;

        return (
            <div className={`CurrentTimeBox ${isEditable ? 'inactive' : ''}`}>
                <h1>{title}</h1>
                <Clock
                    minutes={minutesLeft}
                    seconds={secondsLeft}
                    isPaused={isPaused}
                    color='rainbow'
                />
                <ProgressBar
                    percent={progressInPercent}
                    isPaused={isPaused}
                    big
                    color='green'
                />
                <button onClick={onEdit} disabled={isEditable || isRunning}>Edytuj</button>
                <button onClick={this.handleStart} disabled={isRunning || isEditable}>Start</button>
                <button onClick={this.handleStop} disabled={!isRunning || isEditable}>Stop</button>
                <button onClick={this.togglePause} disabled={!isRunning || isEditable}>{ isPaused ? 'Wznów' : 'Pauzuj' }</button>
                Liczba przerw: {pausesCount}
            </div>
        );
    }
}

export default CurrentTimeBox;

import React from 'react';
import classNames from 'classnames';

/*********************************************************************
******************** Functional Component "Clock" ********************
**********************************************************************/
function Clock({minutes = 0, seconds = 0, color = '', isPaused = false }) {

    const zeroPad = (num, places) => String(num).padStart(places, '0');

    minutes = Math.max(Math.min(minutes, 59), 0);
    seconds = Math.max(Math.min(seconds, 59), 0);

    const clockClassNames = classNames(
        'clock', {
            'inactive': isPaused,
            'clock--color-rainbow': color === 'rainbow',
        },
    )

    return (
        <h2 className={clockClassNames}>
            {'Pozostało '}
            <span className='clock__hours'>{zeroPad(minutes, 2)}</span>
            <span className='clock__separator'>:</span>
            <span className='clock__minutes'>{zeroPad(seconds, 2)}</span>
        </h2>
    );
}

export default Clock;

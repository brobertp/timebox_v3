import React from 'react';

function Timebox({ title, totalTimeInMinutes, onDelete, onEdit }) {

    const [ isEditMode, toggleEditMode ] = React.useState(false);
    const [ changedTitle, changeTitle] = React.useState(title);
    const [ changedTotalTimeInMinutes, changeTotalTimeInMinutes] = React.useState(totalTimeInMinutes);

    const appplyChanges = () => {
        toggleEditMode(false);
        onEdit({
            title: changedTitle,
            totalTimeInMinutes: changedTotalTimeInMinutes,
        });

    }

    const discardChanges = () => {
        toggleEditMode(false);
        changeTitle(title);
        changeTotalTimeInMinutes(totalTimeInMinutes);
    }

    return (
        <div className="Timebox">
            <h3>
                {
                    isEditMode ?
                    (
                        <>
                        <label >Co robisz?
                            <input
                                type="text"
                                value={changedTitle}
                                onChange={event => changeTitle(event.target.value)}
                            />
                        </label>
                        <label >Ile minut?
                            <input
                                type="number"
                                value={changedTotalTimeInMinutes}
                                onChange={event => changeTotalTimeInMinutes(event.target.value)}
                            />
                        </label>
                        </>
                    ) :
                    (`${title} - ${totalTimeInMinutes} min.`)
                }
            </h3>
            <button onClick={onDelete}>Usuń</button>
            {isEditMode ?
                (<>
                    <button onClick={appplyChanges}>Zatwierdź</button>
                    <button onClick={discardChanges}>Odrzuć zmiany</button>
                </>) :
                <button onClick={() => toggleEditMode(true)}>Zmień</button>
            }
        </div>
    )
}

export default Timebox;

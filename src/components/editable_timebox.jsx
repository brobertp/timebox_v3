import React from 'react';

import TimeboxEditor from './timebox_editor';
import CurrentTimeBox from './current_timebox';

/*********************************************************************
*************** Functional Component "EditableTimeBox" ***************
**********************************************************************/
class EditableTimeBox extends React.Component {

    state = {
        title: 'Uczę się wyciągać stan w górę',
        totalTimeInMinutes: 20,
        isEditable: true,
    }

    handleTitleChange = (event) => {
        console.info('>>> handleTitleChange', event.target.value);
        this.setState({ title: event.target.value});
    }

    handleTotalTimeInMinutesChange = (event) => {
        console.info('>>> handleTotalTimeInMinutesChange', event.target.value);
        this.setState({ totalTimeInMinutes: event.target.value});
    }

    handleConfirm = (event) => {
        this.setState({
            isEditable: false,
        })
    }

    handleEdit = (event) => {
        this.setState({
            isEditable: true,
        })
    }

    render() {

        const { title, totalTimeInMinutes, isEditable } = this.state;
        return (
            <>
                <TimeboxEditor
                    isEditable={isEditable}
                    title={title}
                    totalTimeInMinutes={totalTimeInMinutes}
                    onTitleChange={this.handleTitleChange}
                    onTotalTimeInMinutesChange={this.handleTotalTimeInMinutesChange}
                    onConfirm={this.handleConfirm}
                />
                <CurrentTimeBox
                    isEditable={isEditable}
                    title={title}
                    totalTimeInMinutes={totalTimeInMinutes}
                    onEdit={this.handleEdit}
                />
            </>
        );
    }
}

export default EditableTimeBox;

import React from 'react';
import classNames from 'classnames';

/*********************************************************************
***************** Functional Component "ProgressBar" *****************
**********************************************************************/
function ProgressBar({ percent = 33, trackRemaining = false, isPaused = false, big = false, color = ''}) {

    const width = trackRemaining ? 100 - percent : percent;
    const progressClassNames = classNames(
        'progress',
        {
            'inactive': isPaused,
            'progress--big': big,
            'progress--color-red': color === 'red',
            'progress--color-green': color === 'green',
        }
    );
    return (
        <div className={progressClassNames}>
            <div className='progress__bar' style={{ width: `${width}%`}} />
        </div>
    );
}

export default ProgressBar;

import React from "react";

import TimeboxList from './components/timebox_list';
import EditableTimeBox from './components/editable_timebox';


function App() {

    return (
        <div className="App">
            <TimeboxList />
            <EditableTimeBox />
        </div>
    );
}

export default App;

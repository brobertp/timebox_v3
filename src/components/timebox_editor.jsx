import React from 'react';

/*********************************************************************
**************** Functional Component "TimeBoxEditor" ****************
**********************************************************************/
function TimeboxEditor(props) {

    const {
        title,
        totalTimeInMinutes,
        isEditable,
        onTitleChange,
        onTotalTimeInMinutesChange,
        onConfirm,
    } = props;

    return (
        <div className={`TimeboxEditor ${isEditable ? '' : 'inactive'}`}>
            <label >Co robisz?
                <input
                    disabled={!isEditable}
                    type="text"
                    value={title}
                    onChange={onTitleChange}
                />
            </label><br />
            <label >Ile minut?
                <input
                    disabled={!isEditable}
                    type="number"
                    value={totalTimeInMinutes}
                    onChange={onTotalTimeInMinutesChange}
                />
            </label><br/>
            <button
                disabled={!isEditable}
                onClick={onConfirm}
            >
                Zacznij
            </button>
        </div>
    );
}

export default TimeboxEditor;
